socket-activate
===============

This project aims to make it possible to use socket-activated services
on Unix-based systems that don't have systemd installed at all.

It uses the simple [`sd_listen_fds` environment variable and file
descriptor
convention](https://manpages.debian.org/unstable/systemd/sd_listen_fds.3.en.html)
introduced by
[systemd](https://www.freedesktop.org/wiki/Software/systemd/), without
using any code or dependencies from systemd.

Hopefully it will help encourage service software developers that they
can just write simple socket-activated services and not worry about
opening ports (or privilege-dropping, daemonization, etc) themselves,
and those services will Just Work™ on any Unix-based platform.

The current implementation is written in Python 3.

See [the socket-activate manpage](./socket-activate.md) for more
information.


Issues and Code
---------------

Please [report bugs and make merge
requests](https://gitlab.com/dkg/socket-activate)!

Similar work
------------

[systemd](https://www.freedesktop.org/wiki/Software/systemd/) offers
`systemd-socket-activate`, but that code has a few drawbacks that
hopefully this project avoids:

 - The systemd project itself doesn't build on non-Linux systems.
 
 - Using code from systemd may trigger arguments or fights in some
   communities.
 
 - `systemd-socket-activate` doesn't allow for multiple sockets of
   different types.

Author
------

`socket-activate` is written by Daniel Kahn Gillmor
<dkg@fifthhorseman.net> and is licensed under the [GPL version 3 or
above](https://www.gnu.org/licenses/gpl.html)

References
----------

 * [systemd Socket Activation, part 1](http://0pointer.de/blog/projects/socket-activation.html)
 * [systemd Socket Activation, part 2](http://0pointer.de/blog/projects/socket-activation2.html)
 * [Debian bug report asking for non-systemd socket activation](https://bugs.debian.org/922082)
 * [systemd-socket-activate documentation](https://manpages.debian.org/unstable/systemd/systemd-socket-activate.1.en.html)
