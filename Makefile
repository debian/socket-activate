#!/usr/bin/make -f

OBJECTS=socket-activate.1

all: $(OBJECTS)

socket-activate.1: socket-activate.md
	pandoc -s -t man -o $@ $<

clean:
	rm -f $(OBJECTS) tests/expected tests/produced tests/sock

check:
	SOCKET_ACTIVATE=$(CURDIR)/socket-activate ./tests/basic

VERSION ?= $(shell awk '/^version/{ print $$2 }' < NEWS | head -n1)

release:
	git tag -s socket-activate_$(VERSION) -m 'Tagging socket-activate version $(VERSION)' master

.PHONY: clean all test release
