---
title: SOCKET-ACTIVATE
section: 1
header: User Commands
footer: Feb 2019
---

NAME
====

`socket-activate` - open sockets and `exec` a socket-activated service

SYNOPSIS
========

  **socket-activate** [**-v**] [*socketinfo* ...] [**\-\-**] *prog* [*args*]

where *socketinfo* is of the form:

  --*family*:[*option*=*value*,...]:*address*

See `EXAMPLES` below.

DESCRIPTION
===========

The `socket-activate` program opens the requested sockets and then
waits for incoming activity on them.  When this happens, it execs
*prog*, which inherits these open file descriptors.  The first
requested socket will be opened on file descriptor 3, and subsequent
ones follow in order.

If `-v` or `--verbose` is present, `socket-activate` will write
details of what it is doing to stderr before it execs *prog*.

*prog* is the first argument that does not start with a `-`, or the
first argument after a `--`.

SOCKET FAMILIES
===============

*family* can be one of several different values.

For the following values of *family*, if *address* is a single decimal
number, it is treated as a port number and socket-activate will bind to all
available addresses on that port.  If *address* contains a /, the part
before the / is interpreted as a network address of the requested
family, and the part after the / is treated as a port number.

 - `tcp`: TCP on IPv4 and/or IPv6
 - `udp`: UDP on IPv4 and/or IPv6

For the following values of *family*, *address* is interpreted as a
filesystem path.

 - `unix`: Unix-domain streaming socket
 - `unix-dgram`: Unix-domain datagram socket

SOCKET OPTIONS
==============

Each *socketinfo* can have 0 or more options appended to it, separated
by the comma character (`,`) with their value appended after an equals
sign (`=`).

Available options are:

 - `label`: simple text string to identify this socket to the
        inheriting process

 - `backlog`: For streaming sockets (`tcp` and `unix`), the maximum
             queue for pending connections (see listen(2) -- defaults
             to `SOMAXCONN`).

 - `mode`: octal mode for Unix-domain socket (defaults to relying on
          umask(2))

 - `user`: a numeric User ID value for Unix-domain socket ownership

 - `group`: a numeric Group ID value for Unix-domain socket ownership

EXIT CODES
==========

`socket-activate` exits 100 when called with unparseable options.  It prints an
error message to stderr and exits 11 if it cannot successfully bind
all of the requested sockets.  It prints an error message to stderr
and exits with the value of errno if it is unable to exec(3) the
intended process.  Otherwise, its exit code is the same as that of
*prog*.

ENVIRONMENT VARIABLES
=====================

`socket-activate` stores the number of opened file descriptors in the
environment variable `$LISTEN_FDS`, sets `$LISTEN_PID` to getpid(2),
and stores the list of colon-separated labels in `$LISTEN_FDNAMES`.

These environment variables follow the convention understood by
libsystemd (see sd_listen_fds(3)) so it should be usable with any
socket-activated daemon that expects this behavior.

EXAMPLES
========

Run a webserver as a non-privileged user, using privileged ports, and
indicating to the webserver which ports should offer HTTPS:

    socket-activate --tcp::80 --tcp:label=tls:443 -- \
     runuser -u httpd -- \
      webserver

Expose a foregrounded (supervised) pulseaudio daemon to the network:

    socket-activate --tcp:backlog=5:1000 -- \
     pulseaudio --daemonize=no

Run a network daemon only on the IPv4 loopback:

    socket-activate --tcp::127.0.0.1/9999 -- \
     network-daemon

Run a network daemon over a unix-domain socket instead of IP:

    socket-activate --unix::/srv/network-access -- \
     network-daemon

Run a public-facing network daemon with a local socket for the local
administrator to control it:

    socket-activate --tcp::443 --unix:label=control,user=1000,mode=0600:/run/xyz/control -- \
     xyzd


BUGS AND LIMITATIONS
====================

`socket-activate` does not do any sort of name-to-number resolution: network
addresses, user ids, group ids, and port numbers must all be specified
numerically.  Hostnames, user names, group names, service names are
not interpreted.

`socket-activate` produces its error messages in English and is not
internationalized.

`socket-activate` cannot provide a `label` that contains either a colon (`:`)
or a comma (`,`).

`socket-activate` cannot reliably listen on a link-local network address
because it has no way to specify the particular network interface.

`socket-activate` only opens sockets for (and launches) a single
service, so it can't be used for robust system-wide synchronization.
In particular, may be a race condition during startup of concurrent
services.

`socket-activate` doesn't clearly indicate to its caller when the
sockets have been successfully bound, it simply remains foregrounded.

SEE ALSO
========

exec(3), listen(2), runuser(1), sd_listen_fds(3), umask(2)

AUTHOR
======

Daniel Kahn Gillmor <dkg@fifthhorseman.net>
